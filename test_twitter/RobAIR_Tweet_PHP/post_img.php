<?php
require "twitteroauth/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;


if (!defined('CONSUMER_KEY'))
{
    define('CONSUMER_KEY', '');
}

if (!defined('CONSUMER_SECRET'))
{
    define('CONSUMER_SECRET', '');
}

if (!defined('OAUTH_TOKEN'))
{
    define('OAUTH_TOKEN', '');
}

if (!defined('OAUTH_SECRET'))
{
    define('OAUTH_SECRET', '');
}

// Connection
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_SECRET);

// Checking if the authentification was successful
$content = $connection->get('account/verify_credentials');

// Simple tweet
$connection->post('statuses/update', array('status' => 'Tweeting with #RobAIR!'));

// TWEET WITH IMAGE
// Uploading image
$media1 = $connection->upload('media/upload', ['media' => 'cat.jpg']);

// Setting parameters
$parameters = [
    'status' => 'Tweeting with #RobAIR!',
    'media_ids' => implode(',', [$media1->media_id_string]),
];

// Tweeting with image
$result = $connection->post('statuses/update', $parameters);
?>
