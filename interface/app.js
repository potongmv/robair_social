var fs = require('fs'),
  https = require('https'),
  http = require('http'),
  express = require('express'),
  path = require('path'),
  bodyParser = require('body-parser'),
  Twit = require('twit'),
  WebSocketServer = require('ws').Server,

  app = express();
var session = require('express-session')

// Read twitter tokens
var token = fs.readFileSync("twitter_config.json");
var config = JSON.parse(token);
var T = new Twit(config);

//Config file
var data = fs.readFileSync("config.json");
var config = JSON.parse(data);

//Local interface
var ifs = require('os').networkInterfaces();
localAddressTab = Object.keys(ifs).map(x => ifs[x].filter(x => x.family === 'IPv4' && !x.internal)[0]).filter(x => x);
var localAddress = 'localhost';
if (localAddressTab.length > 0) {
  localAddress = localAddressTab[0].address;
}

///////Mini Serveur pour téléchargé l'autorité de certification////////////
apphttp = express();
apphttp.use('/common', express.static('public/common'));
apphttp.get('/', function(req, res) {
  res.header('Content-type', 'text/html');
  return res.end('<h1>Installer le certificat en tant qu\'autorité racine de confiance. Puis redémarez votre navigateur.</h1> <a href="common/rootCA.crt">CA</a> <br/> <a href="https://' + localAddress + ':6080">Contrôle</a>');
});
http.createServer(apphttp).listen(config.httpPort, function(){
  console.log("http listening on port "+config.httpPort);
});

//Session initialisation
app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'fabmstic',
  resave: false,
  httpOnly: true,
  saveUninitialized: true,
  cookie: {
    secure: true
  }
}))

app.use('/', express.static('public/'));
app.get('/', function(req, res) {
  res.header('Content-type', 'text/html');
  console.log(localAddress + " " + req.ip);
  if ("::ffff:" + localAddress == req.ip)
    return res.sendFile(__dirname + '/views/robair/index.html');
  else {
    return res.sendFile(__dirname + '/views/client/index.html');
  }
});
app.get('/local', function(req, res) {
  res.header('Content-type', 'text/html');
  return res.sendFile(__dirname + '/views/client/index.html');
});

serveurHTTPS = https.createServer({
  key: fs.readFileSync(config.ssl.key),
  cert: fs.readFileSync(config.ssl.crt)
}, app).listen(config.httpsPort, function(){
  console.log("https listening on port "+config.httpsPort);
});

// websocket pour envoyer les confirmations de tweet au navigateur
// UPDATE on va plutot utiliser socket.io

/*
var io = require('socket.io')(serveurHTTPS);

io.on('connection', function(socket){
  console.log("un client connecté !")
  socket.on('disconnect', function(){
    console.log('client deconnecté');
  });
});
*/


var websoc = new WebSocketServer({
  server: serveurHTTPS
});

var robairSocket;
var base64Image;
var tweet_txt;

websoc.on("connection", function(ws) {
  console.log("connection");
  ws.on("message", function(str) {
    if (str == "robair") {
      robairSocket = ws;
      console.log("Twitter : robair connecté");
    } else if (str == "tweetOK") {
      console.log("tweet post autorisé");
      // first we must post the media to Twitter
      T.post('media/upload', {
        media_data: base64Image
      }, function(err, data, response) {
        // now we can assign alt text to the media, for use by screen readers and
        // other text-based presentations and interpreters
        var mediaIdStr = data.media_id_string
        var altText = "tweet"
        var meta_params = {
          media_id: mediaIdStr,
          alt_text: {
            text: altText
          }
        }

        T.post('media/metadata/create', meta_params, function(err, data, response) {
          if (!err) {
            // now we can reference the media and post a tweet (media will attach to the tweet)
            var params = {
              status: tweet_txt,
              media_ids: [mediaIdStr]
            }

            T.post('statuses/update', params, function(err, data, response) {
              console.log(data)
            })
          } else {
            console.log(err);
          }
        })
      })
      console.log("end post");
      ws.send("tweetPosted");
    }
  });
  ws.on("close", function() {
    console.log("Client twitter déconnecté.")
  });
});

// Récupération du tweet et demande de confirmation avant le post sur twitter

app.use(bodyParser.json());
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true
}));
app.post('/tweet', function(req, res) {
  // Remove header
  base64Image = req.body.avatar.split(';base64,').pop();
  tweet_txt = req.body.txt;
  var askTweet = {
                    "askTweet" : true,
                    "image" : base64Image
                 };
  var str_image = JSON.stringify(askTweet);
  // sauvegarde de l'image dans tweet.png
  // fs.writeFile('tweet.png', base64Image, {
  //   encoding: 'base64'
  // }, function(err) {
  //   if (err) throw err;
  //   console.log('tweet.png saved');
  // });

  // demande de confirmation de tweet
  try {
    robairSocket.send(str_image);
    console.log("Demande de tweet envoyée");
  } catch (e) { // handle error
    console.log(e);
  }
  res.sendStatus(200);
  res.end();
});
