var ctx;
var video;
var canvastweet;
var askedTweet = false;

function init() {
  console.log("initialisation de twitter.js - robair");
  //var socket = io();
  canvastweet = document.getElementById("canvastweet");
  video = document.getElementById("localVideo");
  ctx = canvastweet.getContext('2d');
  var element = document.getElementById('tweet');
  canvastweet.setAttribute('width', 640);
  canvastweet.setAttribute('height', 480);
  element.addEventListener('click', countdown, false);

  // initialisation des events liees aux boutons yes/no pour la confirmation de tweet
  button_yes = document.getElementsByName('yes');
  button_no = document.getElementsByName('no');
  button_yes[0].addEventListener('click', function(e) {
    console.log('button yes was clicked');
    // reset Image
    var ctx = document.getElementById('canvastweet').getContext("2d");
    ctx.clearRect(0, 0, 640, 480);
    // send command
    ws.send("tweetOK");
    askedTweet = false; // le tweet a été validé, on peut è nouveau faire un autre tweet
    $('#sendconfirm').modal('hide');
  });
  button_no[0].addEventListener('click', function(e) {
    console.log('button no was clicked');
    // reset Image
    var ctx = document.getElementById('canvastweet').getContext("2d");
    ctx.clearRect(0, 0, 640, 480);
    // send command
    ws.send("NOtweet");
    askedTweet = false; // le tweet n'a pas été validé, mais on peut è nouveau faire un autre tweet
    $('#sendconfirm').modal('hide');
  });

  //ce qui se passe si on close le modal
  $('#sendconfirm').on('hidden.bs.modal', function () {
    askedTweet = false;
    console.log("askedTweet = "+askedTweet);
  })

  // websocket pour générer les confirmation de tweet
  var ws = new WebSocket("wss://" + window.location.hostname + ":6080/");
  $(ws).on("open", function(event) {
    console.log("Connexion établie avec le serveur twitter");
    ws.send("robair");
  });
  $(ws).on("message", function(event) {
    console.log(event.originalEvent.data);
    console.log("demande de confirmation de tweet");
    var askTweet = JSON.parse(event.originalEvent.data);
    if (askTweet.askTweet) {
      $('#sendconfirm').modal('show');
      console.log(askTweet);
      var ctx = document.getElementById('canvastweet').getContext("2d");
      var image = new Image();
      image.onload = function(){
        ctx.drawImage(image, 0, 0, 640, 480);
      }
      image.src = "data:image/png;base64," + askTweet.image;
    } else if (event.originalEvent.data == "tweetPosted") {
      ctx.font = "40pt Calibri,Geneva,Arial";
      ctx.fillText("Tweet posted", 10, 60);
    }
  });
  $(ws).on("close", function(event) {
    console.log("Le serveur twitter s'est déconnecté.");
  });
}

function countdown() {
  document.getElementById("text_countdown").textContent = "Smile :) Picture in :";
  $('#countdown').modal('show');
  var timeleft = 5;
  // on intervertit l'affichage des deux videos
  var localVideo = $('#localVideo');
  var remoteVideos = $('#remoteVideos');
  localVideo.attr('id', 'localVideoSwap');
  remoteVideos.attr('id', 'remoteVideosSwap');
  var downloadTimer = setInterval(function () {
    document.getElementById("text_countdown").style.fontSize = "200px";
    document.getElementById("text_countdown").textContent = timeleft;
    if (timeleft <= 0) {  // we let 1 more second
      document.getElementById("text_countdown").textContent = timeleft;
      $('#countdown').modal('hide');
      if (timeleft < 0) {
        document.getElementById("text_countdown").style.fontSize = "60px";
        document.getElementById("text_countdown").textContent = "Smile :) Picture in :";
        clearInterval(downloadTimer);
        // on intervertit l'affichage des deux videos
        var localVideo = $('#localVideoSwap');
        var remoteVideos = $('#remoteVideosSwap');
        localVideo.attr('id', 'localVideo');
        remoteVideos.attr('id', 'remoteVideos');
        tweet();
      }
    }
    timeleft--;
  }, 1000);
}

function countdownRobair(){
  countdown();
}

function tweet() {
  console.log("askedTweet = "+askedTweet);
  if(!askedTweet){ // si on a pas encore une demande de tweet en attente, on en cree une
    askedTweet = true;
    ctx.drawImage(video, 0, 0, 640, 480);
    var txt = document.forms["txt"].elements["txt"].value;

    txt = txt + " #RobAIR";
    var img = canvastweet.toDataURL();
    var data = {};
    data.avatar = img;
    data.txt = txt;
    // Send AJAX post request to /tweet
    $.post('./tweet', data, function(result) {
      console.log('POST request result : ', result);
    });
  }

  /*
  // lorsqu'il y a un message vide, on le signale à l'utilisateur cote serveur
  if (txt == "") {
    ctx.clearRect(0, 0, canvastweet.width, canvastweet.height);
    ctx.font = "40pt Calibri,Geneva,Arial";
    ctx.fillText("Empty message", 10, 60);
  }
  */
}

window.addEventListener('load', init, false);
