function init() {
  console.log("initialisation de twitter.js - client")
  //var socket = io();
  var element = document.getElementById('tweet');
  element.addEventListener('click', tweet, false);
}

$('#exampleModal').on('show.bs.modal', function (event) {
  var modal = $(this)
  var img = document.getElementById('tweet_img');
  img.getContext('2d').drawImage($('#remoteVideos')[0].firstChild, 0, 0, 1500, 1500, 0, 0, 640, 480);
})

function tweeting() {
   var button = $(event.relatedTarget)
   var txt = document.getElementById('message-text').value;
   txt = txt + " #RobAIR";
   var img_ = tweet_img.toDataURL();
   var data = {};
   data.avatar = img_;
   data.txt = txt;
   // Send AJAX post request to /tweet
   $.post('./tweet', data, function (result) {
     console.log('POST request result : ', result);
   });
   $('#exampleModal').modal('hide');
}

function countdown() {
  var timeleft = 5;
  var downloadTimer = setInterval(function () {
    timeleft--;
    document.getElementById("countdowntimer").textContent = "Picture in " + timeleft + " seconds.";
    if (timeleft <= 0) {
      document.getElementById("countdowntimer").textContent = "Smile!"
      clearInterval(downloadTimer);
      $('#exampleModal').modal('show');
    }
  }, 1000);

}
window.addEventListener('load', init, false);
