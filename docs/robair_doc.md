<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/default.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/highlight.min.js"></script>

# Robair Docs
***
## User manual

#### Getting started

###### Required

* Ubuntu 16.04 LTS
* Git
* The current user must have the correct root rights with sudo

###### Procedure

1. <b> Download the repository </b>

 >$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/"identifiant"/robair_social.git RobAIR

2. <b> Launch the installation script </b>

 >$ cd RobAIR <br/>
$ ./scripts/install.bash

  If the current user was not in the dialout group, restart the session
  With the Arduino card plugged in, run the programs.

  >$ robair reload

  The installation script will collect the required packets from the repository and will initially set RobAIR. Some variables will be added to the user's bashrc, plus a systemd service that will allow to restart and stop the programs.

###### Utilization

1. <b> On RobAIR tablet (server side) </b>

  - Turn on the robot
  - Turn on the RobAIR tablet
  - Plug the RobAIR tablet on the robot
  - Be connected to the wifi network (if the robot has an external wifi card, do not forget to disconnect the wifi card (intel) that is included to the robot)

  - Open a terminal and launch the command :
  >$robair ip

  - Note the given IP address (which corresponds to ROBAIR_IP). <br/>
  To launch RobAIR : <br/>
  >$robair start

  - Open the Chromium browser ( $chromium-browser ) and go to https://ROBAIR_IP:6080 (replace ROBAIR_IP by the previous ip address)

2. <b>  On the user machine (client side) </b>

  - Go to  http://ROBAIR_IP:6081

  - Acquire the certification authority and install it on the client browser (if you do not know how to do you can visit [this link]( https://origin-symwisedownload.symantec.com/resources/webguides/sslv/sslva_first_steps/Content/Topics/Configure/ssl_chrome_cert.htm ) .

  - Then go to https://ROBAIR_IP:6080 (sometimes you need to restart your browser before)

######  Management
The RobAIR software part can be handled from the RobAIR tablet with the RobAIR program.
Use the following commands to respectively launch, stop, or restart the RobAIR program :

  >$ robair start

  >$ robair stop

  >$ robair restart

  If systemd was installed (by default on Ubuntu), the following commands drive RobAIR from a systemd service on user session, in order to allow the RobAIR launch during the start-up process and manage the different errors (systemd will restart the program if an error is encountered.
  With some modifications, it should also be possible to configure the activation process by socket.
  The RobAIR program can also be updated with the command :

  >$ robair update

  It will simply recover the last version of RobAIR from Git, recompile and update the Arduino card.


#### The Interfaces

###### Client Side
 To launch the client side RobAIR interface, refer to <b> Utilization > On user machine </b> section. If everything is fine, you should see this interface on your web browser :

<img src="robair_client.png" />

The interface is divided in two main parts.

The first part is where you can see images that RoBAIR is recording with his webcam.
The second part is the options interface, where you can use plenty of options such as:
 - Seeing the state of RoBAIR battery level and connexion quality (Area in green)
 - Making RoBAIR move or rotate remotely (Area in red)
 - Take a picture through RoBAIR cam and tweet it  (Area in yellow)
 - Adjust the video (Area in blue)

      <b> Usage of the twitter button</b>

  The twitter button is very simple to use. Assuming that you want to take a picture of an interesting thing that RoBAIR is targeting with is webcam (it can also be a group of people), all you need to do is pressing the button in form of camera on the Twitter section. <br/>
  Then a counter will start , and at the end a popup containing the image will appear. You will be asking either to post it on twitter, or to cancel.

  <img src="tweet_confirm.png" alt="tweet" />


  If you press the “Tweet” button, a prompt will be displayed on the RoBAIR interface which will invite the person in front of RoBAIR to confirm that he wants the picture to be posted on twitter.
After all this process, the picture you have taken will be published on the RoBAIR twitter account.


###### Server side (on RoBAIR tablet)

To launch the server side RoBAIR interface (on RoBAIR tablet), refer to <b> Utilization > On RoBAIR </b> section.
If everything works fine, you should see an interface which looks like this :

[interface7]

Here you can see images recorded with the client’s cam (if at least one client is connected to RoBAIR).
On the left there is a little option which allows you take and tweet pictures directly via the RoBAIR tablet (the picture will be published with the text entered in the text zone under the twitter button, so you should enter your text before taking the picture).
Sometimes a prompt can appear when a picture is taken by the client on the client PC.


## Developer manual
Hello, Developer !

This part resumes all you need to know in order to add, remove or update functionalities to the RobAIR software.

#### Technologies used in the Project
- Javascript
- NodeJS
- ROS ([rosserial arduino](http://wiki.ros.org/rosserial_arduino/Tutorials), [roslibjs](http://wiki.ros.org/rosbridge_suite/Tutorials/RunningRosbridge))
- HTML
- C++
- Shell Bash


#### Project structure

<img src="project_structure.png" />

RobAIR project is divided in two main parts which are the software interface and the material components.

The robot uses an arduino card to manage these components and
the main source code which commands RobAIR's material components (RobAIR C++ class) is located in the folder _arduino/libraries/robair_, with the entry point in the folder _arduino/robairarduino_.

The interface is served by a nodejs server in the folder _interface_.
The entry point of the interface is _interface/app.js_.

The material side and the interface are linked with the ROS middleware.

In the _scripts_ folder you can find many scripts such as install.sh which installs _robair_ command on a PC. You can also find the robair binary program which is executed when someone launch the robair command.

>install.sh actually puts in the _.bashrc_ file the location of _robair_ and the ip address of the tablet where _robair_ will be executed. If you have two versions of _robair_ on your tablet you can manually modify that location in the _.bashrc_ in order to use the version of _robair_ you want.
[interface10]

>For further information about how these scripts works please refer to te source code (written in bash)

Every generated ssl certificates (when running ./scripts/install.bash ) are in the _ssl_ folder.

In order to test the local modifications, recompile the programs and recharge the Arduino card with the following command
> $ robair reload

#### The interface
<img src="interface_structure.png" />

###### Where to start ?
The RobAIR web interface can be viewed as a website
hosted on the robot tablet. The server is developed in Javascript with [nodejs](https://www.w3schools.com/nodejs/nodejs_intro.asp).

The main code of the server is in the file _app.js_ (so, if you want to turn on the server you have to run the command <b>_node app.js_</b> ). In this file two express applications are written  which listen of connections and serve some web pages. Some configs values are stored in the _configs.json_ file.

###### The interface structure

Depending on the ip address of the computer where you want to view the interface, two different interfaces are showed.
- The client interface

  The html code of the client interface is located in _views/client/index.html_ and the associated javascript files are in _public/client_.

- The RobAIR interface

  The html code of the client interface is located in _views/robair/index.html_ and the associated javascript files are in _public/robair_.

>You can see the client interface in the RobAIR computer by visiting the url https://ROBAIR_IP:6080/local

>Some shared javascript are in the folder _public/common_

#### Some functionalities deeply explained

##### Twitter

This functionality allows RobAIR interact with twitter API, which helps it to publish pictures on its twitter account.

Here, for a better undestanding of the robot environment, we will explain in details how it works. It is easier than you think, trust me. So, let's go !

###### Introduction
  What do we want to do for twitter ? We want three main things:
  - Put a button on the client interace
  - Put a button on the RobAIR interface to allow people in front of the robot to take pictures.
  - Put a physical button on RobAIR which will do the same thing than the virtual button on RobAIR interface.

  But first of all we need some twitter configs to allow nodejs to publish on an account. Follow this [tutorial](https://blog.rapidapi.com/how-to-use-the-twitter-api-in-node-js/) to create a _twitter_config.json_ file in the root of _interface_ folder

1. Button on the client interface <br/>
   We know that the code for the client interface is in _views/client/index.html_. There we have to add the code for the button

   <code class="language-html">
     <div class="panel panel-default">
       <div class="panel-heading">Twitter</div>
       <div class="panel-body">
         <tr>
           <td>
              <button type="button" class="btn btn-default" id="tweet" onclick="countdown()">
                 <span class="glyphicon glyphicon-camera"></span>
               </button>
           </td>
           <td>
               <span id="countdowntimer"></span>
           </td>
           <td>
               <span id="facedetect"></span>
           </td>
       </div>
     </div>
   </code>

   The result looks like the twitter button in the interface

   Let's create a file called _twitter.js_ in the folder _public/client_ where we will write code for handling click events. In this file we write some functions :

   <pre><code>
   function init() {
     console.log("initialisation de twitter.js - client")
     var element = document.getElementById('tweet');
     element.addEventListener('click', tweet, false);
   }
   </code></pre>

   This function get the twitter button by id (his id is "tweet") and attach the "tweet" function as handler for click.

   <pre><code>
   $('#exampleModal').on('show.bs.modal', function (event) {
     var modal = $(this)
     var img = document.getElementById('tweet_img');
     img.getContext('2d').drawImage($('#remoteVideos')[0].firstChild, 0, 0, 1500, 1500, 0, 0, 640, 480);
   })
   </code></pre>

   "#exempleModal" is the id of the window which will popup after the client takes a picture. This code is saying that when the poput appears (show.bs.modal event) the handler fills the "tweet_img" div with the first frame of the "remoteVideos", which is the picture we actually wants to take.

   <pre><code>
   function tweeting() {
      var button = $(event.relatedTarget)
      var txt = document.getElementById('message-text').value;

      txt = txt + " #RobAIR";
      var img_ = tweet_img.toDataURL();
      var data = {};
      data.avatar = img_;
      data.txt = txt;
      // Send AJAX post request to /tweet
      $.post('./tweet', data, function (result) {
        console.log('POST request result : ', result);
      });
      $('#exampleModal').modal('hide');
   }
   </code></pre>

   That function sends "data" (the image to tweet and the text to tweet with) via an http POST request to the server, and hide the modal which is showing the picture and the text.

   <pre><code>
     function countdown() {
       var timeleft = 5;
       var downloadTimer = setInterval(function () {
         timeleft--;
         document.getElementById('text_countdown').textContent = 'Picture in ' + timeleft + ' seconds.';
         <br>
         if (timeleft <=0){  <br>
           document.getElementById("text_countdown").textContent = "Smile!"
           clearInterval(downloadTimer);
           $('#exampleModal').modal('show');
         }
       }, 1000);
     }
   </code></pre>

   That function counts from 5 to 0 and displays digits while counting. When the timer reach 0 it make a modal visible.

   <pre><code>
     window.addEventListener('load', init, false);
   </code></pre>

   We launch the init function whenever the views/client/index.html page is loaded.

   We have to write a peace of code in _app.js_ to handle the http POST request made in the tweeting() function

   <pre><code>
   app.post('/tweet', function(req, res) {
     // Remove header
     base64Image = req.body.avatar.split(';base64,').pop();
     tweet_txt = req.body.txt;

     // demande de confirmation de tweet
     try {
       robairSocket.send("askTweet");
       console.log("Demande de tweet envoyée");
     } catch (e) { // handle error
       console.log(e);
     }
     res.sendStatus(200);
     res.end();
   });
   </code></pre>

   When a http request is sent through /tweet route , the function above is called. That function saves the image and the text to tweet in _base64Image_ and _tweet_txt_ variables (for a further usage maybe). The it sends via webSocket a message to all clients with the label "askTweet". Since RobAIR is also a client, it will receive this message and the will display a confirmation message with the code below (public/robair/twitter.js in init function) :  

   <pre><code>
   // websocket pour générer les confirmation de tweet
   var ws = new WebSocket("wss://" + window.location.hostname + ":6080/");
   $(ws).on("open", function(event) {
     console.log("Connexion établie avec le serveur twitter");
     ws.send("robair");
   });
   $(ws).on("message", function(event) {
     console.log(event.originalEvent.data);
     console.log("demande de confirmation de tweet");
     if (event.originalEvent.data == "askTweet") {
       $('#sendconfirm').modal('show');
     } else if (event.originalEvent.data == "tweetPosted") {
       ctx.font = "40pt Calibri,Geneva,Arial";
       ctx.fillText("Tweet posted", 10, 60);
     }
   });
   </code></pre>

   So we can see that when RobAIR recieves a message he checks his nature. If the label of the message is "askTweet", he shows a confirmation modal to the user . This modal have two buttons , "yes" and "no". When "yes" is clicked, a webSocket message is sent to the server with the label "tweetOK". If the user click "no", a webSocket message is sent to the server with the label "NOtweet". Here is the code (public/robair/twitter.js in init function):

   <pre><code>
   // initialisation des events liees aux boutons yes/no pour la confirmation de tweet
   button_yes = document.getElementsByName('yes');
   button_no = document.getElementsByName('no');
   button_yes[0].addEventListener('click', function(e) {
     console.log('button yes was clicked');
     ws.send("tweetOK");
     askedTweet = false; // le tweet a été validé, on peut è nouveau faire un autre tweet
     $('#sendconfirm').modal('hide');
   });
   button_no[0].addEventListener('click', function(e) {
     console.log('button no was clicked');
     ws.send("NOtweet");
     askedTweet = false; // le tweet n'a pas été validé, mais on peut è nouveau faire un autre tweet
     $('#sendconfirm').modal('hide');
   });

   //ce qui se passe si on close le modal
   $('#sendconfirm').on('hidden.bs.modal', function () {
     askedTweet = false;
     console.log("askedTweet = "+askedTweet);
   })
   </code></pre>

   You can see what the server do when he receives a nessage tagged "tweetOK" and "NOtweet" in app.js :

   <pre><code>
   ws.on("message", function(str) {
     if (str == "robair") {
       robairSocket = ws;
       console.log("Twitter : robair connecté");
     } else if (str == "tweetOK") {
       console.log("tweet post autorisé");
       // first we must post the media to Twitter
       T.post('media/upload', {
         media_data: base64Image
       }, function(err, data, response) {
         // now we can assign alt text to the media, for use by screen readers and
         // other text-based presentations and interpreters
         var mediaIdStr = data.media_id_string
         var altText = "tweet"
         var meta_params = {
           media_id: mediaIdStr,
           alt_text: {
             text: altText
           }
         }

         T.post('media/metadata/create', meta_params, function(err, data, response) {
           if (!err) {
             // now we can reference the media and post a tweet (media will attach to the tweet)
             var params = {
               status: tweet_txt,
               media_ids: [mediaIdStr]
             }

             T.post('statuses/update', params, function(err, data, response) {
               console.log(data)
             })
           } else {
             console.log(err);
           }
         })
       })
       console.log("end post");
       ws.send("tweetPosted");
     }
   });
   </code></pre>

   2. Button on the RobAIR interface

   All the code is in _views/robair/index.html_ and _public/robair/twitter.js_ . The principe is the same as below , except there is no confirmation stape since the user is directly taken the picture. If you have understood the explanations below, this part will be a peace of cake to you :wink:.

   3. Physical button

   The physical button on RobAIR is just another launcher of the tweet() function. So, we just have to detect when someone his pressing the button and then activate the process of tweeting.

   We connect the button to the PIN 7 of the RobAIR ARDUINO card, the we write in c++ the publisher that publish the state of the button (1 for pushed and 0 for released) code in _robair_social/arduino/libraries/robair_.

   <pre><code>
     // =========================  PICTURE BUTTON  =========================

     void Robair::pushButtonPicture()
     {
      //sauvegarde de l'ancien etat d bouton
     	boolean oldPictureState = pictureState;
     	if (digitalRead(PIN_PICTURE) == LOW) {
     		pictureState = true;
     		picture_msg.data = true;
     	}
     	else {
     		picture_msg.data = false;
     		pictureState = false;
     	}
      //on ne publie que si le bouton a changé d'état
     	if(pictureState != oldPictureState) {
     		// on ne publie que si on a changé d'état
     		picture_pub.publish(&picture_msg);
     	}
     }
   </code></pre>

   Thanks to roslibjs we write a suscriber that launch a javascript function whenever the button change his state (code in _public/common/js/robairos.js_ ):

   <pre><code>
   ///////////picture button/////////
   robairros.takePicture = function(msg) {
     if(msg.data == true) {
       countdown();
     }
   }

   var topic_picture = new ROSLIB.Topic({
     ros: ros,
     name:'/picture',
     messageType: 'std_msgs/Bool'
   })

   topic_picture.subscribe(function(message) {
     robairros.takePicture(message);
   });
   </code></pre>

   msg.data == true means the button is pushed. If so, we call the countdown() function which is in _public/robair/tweeter.js_. This function displays a countdown before taking the picture to tweet. Here is another sequence diagram to illustrate what is going on when someone push the tweeter button :

   [interface15]
